﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using MongoDB.Driver;

namespace weekhooklogger
{
    /// <summary>
    /// Summary description for logger
    /// </summary>
    public class logger : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            bool done = false;   
            string payload = "ContentType: " + context.Request.ContentType + " [NOT VALID] ";
            var dictionary = new Dictionary<string, string>();
            if (context.Request.ContentType == "application/json")
            {
                using(var reader = new StreamReader(context.Request.GetBufferlessInputStream()))
                {
                    payload = reader.ReadToEnd();
                }
                foreach (var str in context.Request.Headers.AllKeys)
                {
                    dictionary.Add(str, context.Request.Headers[str]);
                }
                done = true;

            }

            var c = new MongoClient("mongodb://ma:casa29lu@ds035786.mlab.com:35786/");  
            c.GetServer().GetDatabase("webhooklogger").GetCollection("Logger").Insert
                (
                    new
                    {
                        When = DateTime.UtcNow,
                        Headers = dictionary,
                        Payload = payload
                    }
                );

            context.Response.ContentType = "application/json";
            context.Response.Write("{done:" + done + "}");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿function BackFromOut(token, userid,errocode) {
    window._oauthViewModel._init(token,userid,errocode);
}

function OAuthViewModel()
{
    var self = this;
    self._btnwarning = "btn-warning";
    self._btnsuccess = "btn-success";

    
    self._token = ko.observable("");
    self._userid = ko.observable("");
    self._errorcode = ko.observable("");

    self._init = function(token,userid,errorcode) {
        if (token)
        {
            self._token(token);
            self._userid(userid);
            self.buttonState(self._btnsuccess);
            self.statusMessage(" User connected - " + userid + " - token: " + token);

        } else {

            self.buttonState(self._btnwarning);
            self._token("");
            self._userid("");
            self.statusMessage(" connection error - " + errorcode);
        }
        self._errorcode(errorcode);
    };

    self.openAuth = function (item, event) {
        
            var $this = $(event.target);
            window.open($this.attr("data-href"), '_blank', 'width=800,height=650,scrollbars=1');
        
    };

    self._hasToken = function() {
        if (self._token)
            return true;
        return false;
    };

    self.loadSectionData = function (item, event) {
        var $this = $(event.target);
        var getter = "_" + $this.attr("data-getter");
        self[getter]();
    };

    self._user = function() {
        self._callService('User/' + self._userid() + '/Profile', { token: self._token() }, function(data) { console.log(data); }, function(ee,m) {
            console.log("error");
            console.log(m); console.error(ee); });
    };

    self._activitystream = function() {

    };

    self._trainingprogram = function() {

    };


    self.statusMessage = ko.observable("not connected");

    self.buttonState = ko.observable(self._btnwarning);


    self._callService = function(relUrl, data, success, error) {
        /*/Api/v1/User/1b939e3b-ae3c-e311-a8f9-00059a3c7800/Profile*/
        //$.support.cors = true;

        
        var url = window._baseUrl + relUrl;

        data.serviceurl = url;
        
        $.ajax({
            type: "POST",
            url: '/Home/ServiceCall',
            data: data,
            success: success,
            error: error,
            dataType: "json"
        });


      
        //var xhr = createCORSRequest('POST', url);
        //xhr.setRequestHeader('X-MWAPPS-OAUTHCLIENTID', window._clientId);
        //xhr.send($.param(data));

        //xhr.onload = success;

        //xhr.onerror = error;


    };

};


$(document).ready(function () {
    window._oauthViewModel = new OAuthViewModel();
    ko.applyBindings(_oauthViewModel);
});


function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        // XHR for Chrome/Firefox/Opera/Safari.
        xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
        // XDomainRequest for IE.
        xhr = new XDomainRequest();
        xhr.open(method, url);
    } else {
        // CORS not supported.
        xhr = null;
    }
    return xhr;
}
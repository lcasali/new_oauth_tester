﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using HttpCookie = System.Web.HttpCookie;

namespace WebHost.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string clientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            string clientId = ConfigurationManager.AppSettings["ClientId"];


            ViewBag.ClientId = clientId;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        
        public ActionResult OAuth(string code,string error_code, string state)
        {
            var client = new RestClient();
            client.BaseUrl = ConfigurationManager.AppSettings["ServiceUrl"];
            
            var token = new RequestAccessToken
            {
                client_id = ConfigurationManager.AppSettings["ClientId"],
                client_secret = ConfigurationManager.AppSettings["ClientSecret"],
                code = code,
                grant_type = "authorization_code",
                redirect_uri = "http://oauthclient.mywellness.com/home/OAuth"
            };

             var request = new RestRequest(Method.POST)
             {
                 Resource = "OAuth/58FB87D2-B9C1-45D1-83CE-F92C64E787AF/GetAccessToken",
                 RequestFormat = DataFormat.Json
             };

            request.AddBody(token);

            var r = client.Execute(request);

            var obj = (JObject)JsonConvert.DeserializeObject(r.Content);


            JToken jt;
            if (obj.TryGetValue("access_token",StringComparison.InvariantCultureIgnoreCase,out jt))
            {
           
                var myCookie = new HttpCookie(string.Format("mw_{0}", token.client_id));
                myCookie["accesstoken"] = (string) obj["access_token"];
                myCookie["userid"] = (string) obj["user_id"];
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);

                ViewBag.AccessToken = (string) obj["access_token"];
                ViewBag.UserId = (string)obj["user_id"];
            }
            else
            {
                ViewBag.ErrorCode = (string) obj["error_code"];
                ViewBag.AccessToken = "";
                ViewBag.UserId = "";

            }
            return View();
        }

        [HttpPost]
        public ActionResult ServiceCall()
        {
            return Json(null);
        }
    }
}